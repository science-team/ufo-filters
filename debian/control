Source: ufo-filters
Maintainer: Debian PaN Maintainers <debian-pan-maintainers@alioth-lists.debian.net>
Uploaders: Picca Frédéric-Emmanuel <picca@debian.org>,
 Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>,
 Serge Cohen <serge1cohen@free.fr>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 12),
               cmake,
               libclfft-dev,
               libglib2.0-dev,
               libgsl0-dev | libgsl-dev,
               libhdf5-dev,
               libtiff-dev,
               libufo-dev (>= 0.16),
               ocl-icd-opencl-dev,
               python3,
               python3-sphinx
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/science-team/ufo-filters
Vcs-Git: https://salsa.debian.org/science-team/ufo-filters.git
Homepage: http://ufo.kit.edu/

Package: ufo-filters
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         ufo-filters-data (= ${source:Version})
Suggests: ufo-filters-doc
Description: Set of plugins for ufo-core - runtime
 The UFO data processing framework is a C library suited to build
 general purpose streams data processing on heterogeneous
 architectures such as CPUs, GPUs or clusters. It is extensively used
 at the Karlsruhe Institute of Technology for Ultra-fast X-ray Imaging
 (radiography, tomography and laminography).
 .
 This package contains `average', `backproject', `bin', `blur', `buffer',
 `calculate', `camera', `clip', `contrast', `crop', `denoise', `duplicate',
 `fftmult', `fft', `filter', `flatten', `flip', `forwardproject', `gemm',
 `ifft', `interpolate', `loop', `measure', `merge', `metaballs', `monitor',
 `null', `opencl', `ordfilt', `pad', `read', `reduce', `refeed', `replicate',
 `rescale', `ringwriter', `sleep', `slice', `stack', `stdin', `stdout',
 `subtract', `transpose', `write' and `zeropad' plugins

Package: ufo-filters-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
         libjs-mathjax
Description: Library for high-performance, GPU-based computing - documentation
 The UFO data processing framework is a C library suited to build
 general purpose streams data processing on heterogeneous
 architectures such as CPUs, GPUs or clusters. It is extensively used
 at the Karlsruhe Institute of Technology for Ultra-fast X-ray Imaging
 (radiography, tomography and laminography).
 .
 This package provides the documentation for the plugins

Package: ufo-filters-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Library for high-performance, GPU-based computing - data
 The UFO data processing framework is a C library suited to build
 general purpose streams data processing on heterogeneous
 architectures such as CPUs, GPUs or clusters. It is extensively used
 at the Karlsruhe Institute of Technology for Ultra-fast X-ray Imaging
 (radiography, tomography and laminography).
 .
 This package provides the OpenCL kernel files for the plugins
