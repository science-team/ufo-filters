/*
 * Binning voxels according to various available kernels/functions.
 * This file is part of ufo-serge filter set.
 * Copyright (C) 2018 Serge Cohen
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Serge Cohen <serge.cohen@synchrotron-soleil.fr>
 */

#include <stdio.h>
#include <math.h>

#include <ufo/ufo-cl.h>
#include "ufo-bin-voxels-task.h"

// The enumeration of available kernels
typedef enum {
  KER_MIN,
  KER_MAX,
  KER_RANGE,
  KER_AVERAGE,
  KER_VAR
} Kernel;

// And the corresponding naming of these
static GEnumValue kernel_values[] = {
  { KER_MIN, "KER_MIN", "min" },
  { KER_MAX, "KER_MAX", "max" },
  { KER_RANGE, "KER_RANGE", "range"},
  { KER_AVERAGE, "KER_AVERAGE", "average"},
  { KER_VAR, "KER_VAR", "variance"},
  { 0, NULL, NULL}
};

struct _UfoBinVoxelsTaskPrivate {
  cl_context context;
  Kernel     kernel_choice;
  guint      edge_size;
  cl_kernel  kernel_stream; //!< The kernel to /accumulate/ frames within a binning substack
  cl_kernel  kernel_reduce; //!< The kernel to reduce the result when reached the end of a binning substack
  guint32    in_w; //!< The width of the input image
  guint32    in_h; //!< The height of the input image
  guint32    out_w; //!< The width of the output image
  guint32    out_h; //!< The height of the output image
  cl_mem     buff_1; // Might need up to 2 buffers to store intermediate results while working
  cl_mem     buff_2; // on a binning substack (using cl_mem since those are NOT the output)
  guint32    proc_frame;
  guint32    gene_frame;
};

#warning Check that indeed buff_1 and buff_2 are not used as if they were (cl_mem*) (they are (cl_mem)).

static void ufo_task_interface_init (UfoTaskIface *iface);

G_DEFINE_TYPE_WITH_CODE (UfoBinVoxelsTask, ufo_bin_voxels_task, UFO_TYPE_TASK_NODE,
                         G_IMPLEMENT_INTERFACE (UFO_TYPE_TASK,
                                                ufo_task_interface_init)
			 G_ADD_PRIVATE(UfoBinVoxelsTask))

#define UFO_BIN_VOXELS_TASK_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), UFO_TYPE_BIN_VOXELS_TASK, UfoBinVoxelsTaskPrivate))

enum {
  PROP_0,
  PROP_SIZE,
  PROP_KERNEL,
  N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES] = { NULL, };

UfoNode *
ufo_bin_voxels_task_new (void)
{
  return UFO_NODE (g_object_new (UFO_TYPE_BIN_VOXELS_TASK, NULL));
}

// Only called in once, at the very start
static void
ufo_bin_voxels_task_setup (UfoTask *task,
                           UfoResources *resources,
                           GError **error)
{
  // Likely complete:
  UfoBinVoxelsTaskPrivate *priv = UFO_BIN_VOXELS_TASK_GET_PRIVATE(task);
  char kernel_opts[1024];

  // Preparing macros to setup the OpenCL kernels :
  snprintf(kernel_opts, 1023, "-DEDGE=%u",
           priv->edge_size);

  // Towards OpenCL kernels, first get context :
  priv->context = ufo_resources_get_context(resources);
  UFO_RESOURCES_CHECK_CLERR(clRetainContext(priv->context));

  // Then build the streaming/processing kernel :
  switch (priv->kernel_choice) {
  case KER_MIN:
  case KER_MAX:
  case KER_RANGE:
    priv->kernel_stream =
      ufo_resources_get_kernel(resources,
                               "bin-voxels.cl", "range_k",
                               kernel_opts, error);
    break;
  case KER_AVERAGE:
  case KER_VAR:
    priv->kernel_stream =
      ufo_resources_get_kernel(resources,
                               "bin-voxels.cl", "ave_var_k",
                               kernel_opts, error);
    break;
  default:
    break;
  }
  if ( NULL != priv->kernel_stream )
    UFO_RESOURCES_CHECK_CLERR(clRetainKernel(priv->kernel_stream));

  // Then build the reducing/generating kernel :
  switch (priv->kernel_choice) {
  case KER_MIN:
    priv->kernel_reduce =
      ufo_resources_get_kernel(resources,
                               "bin-voxels.cl", "min_red_k",
                               kernel_opts, error);
    break;
  case KER_MAX:
    priv->kernel_reduce =
      ufo_resources_get_kernel(resources,
                               "bin-voxels.cl", "max_red_k",
                               kernel_opts, error);
    break;
  case KER_RANGE:
    priv->kernel_reduce =
      ufo_resources_get_kernel(resources,
                               "bin-voxels.cl", "range_red_k",
                               kernel_opts, error);
    break;
  case KER_AVERAGE:
    priv->kernel_reduce =
      ufo_resources_get_kernel(resources,
                               "bin-voxels.cl", "ave_red_k",
                               kernel_opts, error);
    break;
  case KER_VAR:
    priv->kernel_reduce =
      ufo_resources_get_kernel(resources,
                               "bin-voxels.cl", "var_red_k",
                               kernel_opts, error);
    break;
  default:
    break;
  }
  if ( NULL != priv->kernel_reduce )
    UFO_RESOURCES_CHECK_CLERR(clRetainKernel(priv->kernel_reduce));

}

static void
ufo_bin_voxels_task_get_requisition (UfoTask *task,
                                     UfoBuffer **inputs,
                                     UfoRequisition *requisition,
                                     GError **error)
{
  // Likely complete:
  UfoBinVoxelsTaskPrivate *priv = UFO_BIN_VOXELS_TASK_GET_PRIVATE(task);
  UfoRequisition in_req;

  /* getting the requisition of the first input */
  ufo_buffer_get_requisition(inputs[0], &in_req);

  /* Taking care of the requisition itself */
  requisition->n_dims = 2;
  requisition->dims[0] = in_req.dims[0] / priv->edge_size;
  requisition->dims[1] = in_req.dims[1] / priv->edge_size;

  /* logging for the development */
  cl_uint inFrameInBox = priv->proc_frame % priv->edge_size;
  /*
  printf("bin-voxels (%s.%d) : about to process frame %u (%lu x %lu), indexed %u/%u within binning-box."
         " Binned image should be %u x %u (requisition : %lu x %lu).\n",
	 __func__, __LINE__,
         priv->proc_frame, in_req.dims[0], in_req.dims[1],
         inFrameInBox, priv->edge_size, priv->out_w, priv->out_h,
         requisition->dims[0], requisition->dims[1]);
  fflush(stdout);
  */

  /* Storing/checking there is no change since last call : */
  if ( (requisition->dims[0] != priv->out_w) && (0 != priv->out_w) ) {
    g_error("In task 'bin-voxels' : frame width of the sreeam has changed while processing.\n"
            "Was %u and requested to change to %lu\n", priv->out_w, requisition->dims[0]);
  }
  if ( (requisition->dims[1] != priv->out_h) && (0 != priv->out_h) ) {
    g_error("In task 'bin-voxels' : frame height of the sreeam has changed while processing.\n"
            "Was %u and requested to change to %lu\n", priv->out_h, requisition->dims[1]);
  }

  priv->in_w = in_req.dims[0];
  priv->in_h = in_req.dims[1];

  /* If necessary, allocating the buffers */
  if ( NULL == priv->buff_1 ) {
    cl_int errcode;
    size_t buff_size_B;

    priv->out_w = requisition->dims[0];
    priv->out_h = requisition->dims[1];

    buff_size_B = 4 * priv->out_w * priv->out_h; // OpenCL declares float as 4B

    priv->buff_1 = clCreateBuffer(priv->context,
                                  CL_MEM_READ_WRITE | CL_MEM_HOST_NO_ACCESS,
                                  buff_size_B, NULL,
                                  &errcode);
    UFO_RESOURCES_CHECK_CLERR (errcode);

    priv->buff_2 = clCreateBuffer(priv->context,
                                  CL_MEM_READ_WRITE | CL_MEM_HOST_NO_ACCESS,
                                  buff_size_B, NULL,
                                  &errcode);
    UFO_RESOURCES_CHECK_CLERR (errcode);
  }
}

static guint
ufo_bin_voxels_task_get_num_inputs (UfoTask *task)
{
  return 1;
}

static guint
ufo_bin_voxels_task_get_num_dimensions (UfoTask *task,
                                        guint input)
{
  return 2;
}

// Different calls to submit frames (within input stream) and get results -> reductor
/*
 * The `process` function is called for each new input frame. When returning 'TRUE' the
 * scheduler moves to the next input frame /directly/, when returning 'FALSE' the scheduler
 * will call the `generate` method to get single output frame.
 *
 * The `generate`function can also return 'FALSE', meaning that it has no extra output
 * frame to provide to the scheduler, or 'TRUE', meaning one more frame (at least) can be
 * provided right away by `generate`.
 *
 * `generate` will be called every time `process` returns 'FALSE' **or** there is no more input
 * to provide to `process`.
 *
 * Indeed the functional mechanism used by a scheduler on TASK_REDUCTOR can be seen in
 * /ie./ ufo-fixed-scheduler.c reduce_loop function.
 */
static UfoTaskMode
ufo_bin_voxels_task_get_mode (UfoTask *task)
{
  return UFO_TASK_MODE_REDUCTOR | UFO_TASK_MODE_GPU;
}

/* NB : the provided requisition is the one produced by the task by the
 * xx_get_requisition function. Hence it is also the proper dimension for
 * the organisation of the work-items of the `process` sub-task.
 */
static gboolean
ufo_bin_voxels_task_process (UfoTask *task,
                             UfoBuffer **inputs,
                             UfoBuffer *output,
                             UfoRequisition *requisition)
{
  // Likely complete:
  UfoBinVoxelsTaskPrivate *priv = UFO_BIN_VOXELS_TASK_GET_PRIVATE(task);
  UfoGpuNode *node = UFO_GPU_NODE(ufo_task_node_get_proc_node(UFO_TASK_NODE(task)));
  UfoProfiler *profiler = ufo_task_node_get_profiler(UFO_TASK_NODE(task));
  cl_command_queue cmd_queue = ufo_gpu_node_get_cmd_queue(node);

  cl_mem in_frame_mem = ufo_buffer_get_device_array (inputs[0], cmd_queue);

  cl_uint inFrameInBox = priv->proc_frame % priv->edge_size;
  cl_uint inXSize, inYSize; // Taken from the inpur image
  UfoRequisition in_frame_req;

  ufo_buffer_get_requisition (inputs[0], &in_frame_req);

  inXSize = in_frame_req.dims[0];
  inYSize = in_frame_req.dims[1];

  // Ready to prepare the kernel execution : setting the parameters
  cl_kernel kernel = priv->kernel_stream;

  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 0, sizeof (cl_mem), &in_frame_mem));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 1, sizeof (cl_mem), &(priv->buff_1)));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 2, sizeof (cl_mem), &(priv->buff_2)));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 3, sizeof (cl_uint), &inFrameInBox));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 4, sizeof (cl_uint), &inXSize));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 5, sizeof (cl_uint), &inYSize));

  // Some loging, at least during development work :
  /*
  printf("bin-voxels : about to process frame %u (%u x %u), indexed %u/%u within binning-box."
         " Binned image should be %u x %u (requisition : %lu x %lu).\n",
         priv->proc_frame, inXSize, inYSize,
         inFrameInBox, priv->edge_size, priv->out_w, priv->out_h,
         requisition->dims[0], requisition->dims[1]);
  fflush(stdout);
  */

  // And calling the kernel (within the profiler) :
  ufo_profiler_call (profiler, cmd_queue, kernel, requisition->n_dims, requisition->dims, NULL); // one work item per output pixel

  // Finally increment the number of processed input frames
  // and, when necessary, triger the generation of a binned frame :
  priv->proc_frame += 1;
  if ( 0 != (priv->proc_frame % priv->edge_size) ) {
    // printf("%s.%d returning TRUE\n", __func__, __LINE__);
    return TRUE; // We can go-on `processing` without `generating`.
  }
  else {
    // printf("%s.%d returning FALSE\n", __func__, __LINE__);
    return FALSE; // We have to `generate` a binned frame before coninuing the processing.
  }
}

/*
 * Always returning FALSE, since there is no use of calling twice `generate` without calling
 * at least once `process` inbetween.
 */
static gboolean
ufo_bin_voxels_task_generate (UfoTask *task,
                              UfoBuffer *output,
                              UfoRequisition *requisition)
{
  // Likely complete:
  UfoBinVoxelsTaskPrivate *priv = UFO_BIN_VOXELS_TASK_GET_PRIVATE(task);

  /*
  printf("enterring %s.%d proc_frame=%u, gene_frame=%u (gene_frame*dge_size=%u)\n",
	 __func__, __LINE__,
	 priv->proc_frame, priv->gene_frame,
	 (priv->edge_size * priv->gene_frame)
	 );
  */

  /* Do we have to generate a new binned frame (or just done) */
  if ( (priv->edge_size * priv->gene_frame) > priv->proc_frame ) {
    // printf("%s.%d returning FALSE\n", __func__, __LINE__);
    return FALSE;
  }

  UfoGpuNode *node = UFO_GPU_NODE(ufo_task_node_get_proc_node(UFO_TASK_NODE(task)));
  UfoProfiler *profiler = ufo_task_node_get_profiler(UFO_TASK_NODE(task));
  cl_command_queue cmd_queue = ufo_gpu_node_get_cmd_queue(node);

  cl_mem out_bin_mem = ufo_buffer_get_device_array (output, cmd_queue);
  cl_uint inBoxDepth = priv->proc_frame % priv->edge_size;
  cl_uint inXSize, inYSize; // Size of input image, recorded in priv :

  inBoxDepth = (0 == inBoxDepth)? priv->edge_size : inBoxDepth;
  inXSize = priv->in_w;
  inYSize = priv->in_h;

  // Ready to prepare the kernel execution : setting the parameters
  cl_kernel kernel = priv->kernel_reduce;

  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 0, sizeof (cl_mem), &out_bin_mem));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 1, sizeof (cl_mem), &(priv->buff_1)));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 2, sizeof (cl_mem), &(priv->buff_2)));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 3, sizeof (cl_uint), &inBoxDepth));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 4, sizeof (cl_uint), &inXSize));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 5, sizeof (cl_uint), &inYSize));

  // Some loging, at least during development work :
  /*
  printf("bin-voxels : about to generate frame %u (%u x %u; requisition : %lu x %lu), depth %u/%u.\n",
         priv->gene_frame,  priv->out_w, priv->out_h,
         requisition->dims[0], requisition->dims[1],
         inBoxDepth, priv->edge_size);
  fflush(stdout);
  */

  // And calling the kernel (within the profiler) :
  ufo_profiler_call (profiler, cmd_queue, kernel, requisition->n_dims, requisition->dims, NULL); // one work item per output pixel

  priv->gene_frame += 1;
  // printf("%s.%d returning TRUE\n", __func__, __LINE__);
  return TRUE;
}

static void
ufo_bin_voxels_task_set_property (GObject *object,
                                  guint property_id,
                                  const GValue *value,
                                  GParamSpec *pspec)
{
  UfoBinVoxelsTaskPrivate *priv = UFO_BIN_VOXELS_TASK_GET_PRIVATE (object);

  switch (property_id) {
  case PROP_SIZE:
    priv->edge_size = g_value_get_uint(value);
    break;
  case PROP_KERNEL:
    priv->kernel_choice = g_value_get_enum(value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
ufo_bin_voxels_task_get_property (GObject *object,
                                  guint property_id,
                                  GValue *value,
                                  GParamSpec *pspec)
{
  UfoBinVoxelsTaskPrivate *priv = UFO_BIN_VOXELS_TASK_GET_PRIVATE (object);

  switch (property_id) {
  case PROP_SIZE:
    g_value_set_uint(value, priv->edge_size);
    break;
  case PROP_KERNEL:
    g_value_set_enum(value, priv->kernel_choice);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
ufo_bin_voxels_task_finalize (GObject *object)
{
  // Getting the instance objects to release :
  UfoBinVoxelsTaskPrivate *priv = UFO_BIN_VOXELS_TASK_GET_PRIVATE(object);

  priv->out_w = 0;
  priv->out_h = 0;

  if ( priv->buff_1 ) {
    UFO_RESOURCES_CHECK_CLERR(clReleaseMemObject(priv->buff_1));
    priv->buff_1 = NULL;
  }
  if ( priv->buff_2 ) {
    UFO_RESOURCES_CHECK_CLERR(clReleaseMemObject(priv->buff_2));
    priv->buff_2 = NULL;
  }

  if ( priv->kernel_stream ) {
    UFO_RESOURCES_CHECK_CLERR(clReleaseKernel(priv->kernel_stream));
    priv->kernel_stream = NULL;
  }
  if ( priv->kernel_reduce ) {
    UFO_RESOURCES_CHECK_CLERR(clReleaseKernel(priv->kernel_reduce));
    priv->kernel_reduce = NULL;
  }
  if ( priv->context ) {
    UFO_RESOURCES_CHECK_CLERR(clReleaseContext(priv->context));
    priv->context = NULL;
  }
  // Finally, releasing the /super/ :
  G_OBJECT_CLASS (ufo_bin_voxels_task_parent_class)->finalize (object);
}

static void
ufo_task_interface_init (UfoTaskIface *iface)
{
  iface->setup = ufo_bin_voxels_task_setup;
  iface->get_num_inputs = ufo_bin_voxels_task_get_num_inputs;
  iface->get_num_dimensions = ufo_bin_voxels_task_get_num_dimensions;
  iface->get_mode = ufo_bin_voxels_task_get_mode;
  iface->get_requisition = ufo_bin_voxels_task_get_requisition;
  iface->process = ufo_bin_voxels_task_process;
  iface->generate = ufo_bin_voxels_task_generate;
}

static void
ufo_bin_voxels_task_class_init (UfoBinVoxelsTaskClass *klass)
{
  GObjectClass *oclass = G_OBJECT_CLASS (klass);

  oclass->set_property = ufo_bin_voxels_task_set_property;
  oclass->get_property = ufo_bin_voxels_task_get_property;
  oclass->finalize = ufo_bin_voxels_task_finalize;

  properties[PROP_SIZE] =
    g_param_spec_uint ("size",
                       "The edge size of the box to bin",
                       "The edge size of the box to bin. Each 3D box of this edge size is replaced by a single voxel, through a function defined by the `value` parameter",
                       +2, +16, +2, // min, max, default
                       G_PARAM_READWRITE);

  properties[PROP_KERNEL] =
    g_param_spec_enum ("value",
                       "Which value should be used to summarised the binned box, one of (\"min\", \"max\", \"range\", \"average\", \"var\")",
                       "Which value should be used to summarised the binned box, one of (\"min\", \"max\", \"range\", \"average\", \"var\"). With this task, each box of pixel is replaed by a single value. This property selects the applied function to get this value.",
                       g_enum_register_static ("value", kernel_values),
                       KER_MIN, G_PARAM_READWRITE);



  for (guint i = PROP_0 + 1; i < N_PROPERTIES; i++)
    g_object_class_install_property (oclass, i, properties[i]);

  //  g_type_class_add_private (oclass, sizeof(UfoBinVoxelsTaskPrivate));
}

static void
ufo_bin_voxels_task_init(UfoBinVoxelsTask *self)
{
  UfoBinVoxelsTaskPrivate *priv;
  self->priv = priv = UFO_BIN_VOXELS_TASK_GET_PRIVATE(self);

  // Some initialisation :
  priv->context = NULL;
  priv->kernel_stream = NULL;
  priv->kernel_reduce = NULL;
  priv->in_w = 0;
  priv->in_h = 0;
  priv->out_w = 0;
  priv->out_h = 0;
  priv->buff_1 = NULL;
  priv->buff_2 = NULL;
  priv->proc_frame = 0;
  priv->gene_frame = 0;
}
