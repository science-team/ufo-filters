/*
 * Removing vertical stripes in sinogram to reduce ring artefacts.
 * This file is part of ufo-serge filter set.
 * Copyright (C) 2018 Serge Cohen
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Serge Cohen <serge.cohen@synchrotron-soleil.fr>
 */

#include <stdio.h>
#include <math.h>

#include <ufo/ufo-cl.h>
#include "ufo-destrip-faster-task.h"

#ifndef M_PI_2
#define M_PI_2         1.57079632679489661923  /* pi/2 */
#endif

#warning Might be usefull to be able to get the distance to MED in MAD unit for each pixel
// Should be able to perform the correction either in scale or translation (depending
// if sinograms are in transmitance or absorbance).
//
// Also should be able (at least) to perform : raw-correction, clipped correction (hard-clipping)
// some kind of shot-clipped correction (based on tanh seems better).
// Later on should be able to set some kind of hardness to the /soft-clipping/
//
// That might be a useful tool to set appropriate threshold ?
//

// Types to select output :
typedef enum {
  OUTPUT_DESTRIPE,
  OUTPUT_DESTRIPE_RAW,
  OUTPUT_AVERAGE,
  OUTPUT_SMOOTHED,
  OUTPUT_CLIPPED_CORRECTION,
  OUTPUT_RAW_CORRECTION
} Output;

static GEnumValue output_values[] = {
  { OUTPUT_DESTRIPE, "OUTPUT_DESTRIPE", "destriped" },
  { OUTPUT_DESTRIPE_RAW, "OUTPUT_DESTRIPE_RAW", "destriped-raw" },
  { OUTPUT_AVERAGE, "OUTPUT_AVERAGE", "average" },
  { OUTPUT_SMOOTHED, "OUTPUT_SMOOTHED", "smooth"},
  { OUTPUT_CLIPPED_CORRECTION, "OUTPUT_CLIPPED_CORRECTION", "clipped-correction"},
  { OUTPUT_RAW_CORRECTION, "OUTPUT_RAW_CORRECTION", "raw-correction"},
  { 0, NULL, NULL}
};

// Types to choose correction mode
typedef enum {
  CORRECT_ADDITIVE,
  CORRECT_MULTIPLICATIVE
} CorrectionMode;

static GEnumValue correction_mode_values[] = {
  { CORRECT_ADDITIVE, "CORRECT_ADDITIVE", "additive" },
  { CORRECT_MULTIPLICATIVE, "CORRECT_MULTIPLICATIVE", "multiplicative" },
  { 0, NULL, NULL}
};

struct _UfoDestripFasterTaskPrivate {
  cl_context context;
  cl_kernel run_average_kernel;
  cl_kernel smooth_proj_kernel;
  cl_kernel correct_sino_kernel;
  cl_mem averaged; // To be used only when this is NOT the output (otherwise use the Ufo provided buffer)
  cl_mem smoothed; // To be used only when this is NOT the output (otherwise use the Ufo provided buffer)
  gfloat threshold;
  gfloat max_correction;
  guint width;
  guint length;
  Output output;
  CorrectionMode mode;
};

static void ufo_task_interface_init (UfoTaskIface *iface);

G_DEFINE_TYPE_WITH_CODE (UfoDestripFasterTask, ufo_destrip_faster_task, UFO_TYPE_TASK_NODE,
                         G_IMPLEMENT_INTERFACE (UFO_TYPE_TASK,
                                                ufo_task_interface_init)
			 G_ADD_PRIVATE(UfoDestripFasterTask))

#define UFO_DESTRIP_FASTER_TASK_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), UFO_TYPE_DESTRIP_FASTER_TASK, UfoDestripFasterTaskPrivate))

enum {
  PROP_0,
  PROP_THRESHOLD,
  PROP_MAX_CORRECTION,
  PROP_WIDTH,
  PROP_LENGTH,
  PROP_OUTPUT,
  PROP_MODE,
  N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES] = { NULL, };

UfoNode *
ufo_destrip_faster_task_new (void)
{
  // OK ?
  return UFO_NODE (g_object_new (UFO_TYPE_DESTRIP_FASTER_TASK, NULL));
}

static void
ufo_destrip_faster_task_setup (UfoTask *task,
                                 UfoResources *resources,
                                 GError **error)
{
  UfoDestripFasterTaskPrivate *priv;
  char kernel_opts[1024];

  priv = UFO_DESTRIP_FASTER_TASK_GET_PRIVATE (task);

  if ( OUTPUT_DESTRIPE != priv->output ) { // Only printing out when not 'standard/filtered' output is requested.
    fprintf(stderr, "In %s.%d\n", __func__, __LINE__);
    fprintf(stderr, "  threshold = %f\n", priv->threshold);
    fprintf(stderr, "  max-correction = %f\n", priv->max_correction);
    fprintf(stderr, "  width = %u\n", priv->width);
    fprintf(stderr, "  length = %u\n", priv->length);
    fprintf(stderr, "  output = %u\n", priv->output);
    fprintf(stderr, "  correction mode = %u\n", priv->mode);
    fprintf(stderr, "\n");
  }

  // Preparing macros to setup the OpenCL kernels :
  /*
  snprintf(kernel_opts, 1023, "-DMED_LENGTH=%u -DMED_BUFF_SIZE=%u -DCORRECTED_SINO=%u",
           priv->width,
           ((priv->width+1)<<1),
           (OUTPUT_CORRECTION == priv->output) ? 0 : 1);
  // */

  snprintf(kernel_opts, 1023, "-DMED_LENGTH=%u -DSINO_OUTPUT=%u -DCOR_MODE=%u",
           priv->width,
           priv->output,
	   priv->mode);
  // */

  // Building OpenCL kernels :
  priv->context = ufo_resources_get_context(resources);
  UFO_RESOURCES_CHECK_CLERR(clRetainContext(priv->context));

  priv->run_average_kernel =
    ufo_resources_get_kernel(resources,
                             "destrip-faster.cl", "run_average",
                             kernel_opts, error);
  if ( NULL != priv->run_average_kernel )
    UFO_RESOURCES_CHECK_CLERR(clRetainKernel(priv->run_average_kernel));

  priv->smooth_proj_kernel =
    ufo_resources_get_kernel(resources,
                             "destrip-faster.cl", "smooth_proj",
                             kernel_opts, error);
  if ( NULL != priv->smooth_proj_kernel )
    UFO_RESOURCES_CHECK_CLERR(clRetainKernel(priv->smooth_proj_kernel));

  priv->correct_sino_kernel =
    ufo_resources_get_kernel(resources,
                             "destrip-faster.cl", "correct_sino",
                             kernel_opts, error);
  if ( NULL != priv->correct_sino_kernel )
    UFO_RESOURCES_CHECK_CLERR(clRetainKernel(priv->correct_sino_kernel));
}

static void
ufo_destrip_faster_task_get_requisition (UfoTask *task,
                                           UfoBuffer **inputs,
                                           UfoRequisition *requisition,
                                           GError **error)
{
  // Likely not complete ?

  UfoRequisition in_req;
  size_t req_size_B;
  UfoDestripFasterTaskPrivate *priv;
  priv = UFO_DESTRIP_FASTER_TASK_GET_PRIVATE (task);

  ufo_buffer_get_requisition (inputs[0], &in_req);
  /* Taking care of the requisition itself */
  requisition->n_dims = 2;
  requisition->dims[0] = in_req.dims[0];
  requisition->dims[1] = in_req.dims[1];

  req_size_B = sizeof(float) * requisition->dims[0] * requisition->dims[1];

  /* Any other thing to initialise or set here ? */
  // Allocating required intermediate cl_mem :
  if ( (NULL == priv->averaged) && (OUTPUT_AVERAGE != priv->output) ) {
    // Allocating an intermediate cl_mem to store averaged siongram
    cl_int errcode;
    priv->averaged = clCreateBuffer(priv->context,
                                    CL_MEM_READ_WRITE,
                                    req_size_B, NULL,
                                    &errcode);
    UFO_RESOURCES_CHECK_CLERR (errcode);
  }
  if ( (NULL == priv->smoothed) && (OUTPUT_SMOOTHED != priv->output) ) {
    // Allocating an intermediate cl_mem to store smoothed sinogram
    cl_int errcode;
    priv->smoothed = clCreateBuffer(priv->context,
                                    CL_MEM_READ_WRITE,
                                    req_size_B, NULL,
                                    &errcode);
    UFO_RESOURCES_CHECK_CLERR (errcode);
  }
}

static guint
ufo_destrip_faster_task_get_num_inputs (UfoTask *task)
{
  // OK ?
  return 1;
}

static guint
ufo_destrip_faster_task_get_num_dimensions (UfoTask *task,
                                              guint input)
{
  // OK ?
  return 2;
}

static UfoTaskMode
ufo_destrip_faster_task_get_mode (UfoTask *task)
{
  // OK (default but seems not to need any extra) ?
  return UFO_TASK_MODE_PROCESSOR | UFO_TASK_MODE_GPU;
}

static gboolean
ufo_destrip_faster_task_process (UfoTask *task,
                                   UfoBuffer **inputs,
                                   UfoBuffer *output,
                                   UfoRequisition *requisition)
{

  UfoDestripFasterTaskPrivate *priv;
  UfoGpuNode *node;
  UfoProfiler *profiler;
  cl_command_queue cmd_queue;
  cl_kernel kernel;
  cl_mem in_mem;
  cl_mem out_mem;

  guint sino_width = requisition->dims[0];
  guint sino_projs = requisition->dims[1];

  priv = UFO_DESTRIP_FASTER_TASK_GET_PRIVATE(task);
  node = UFO_GPU_NODE (ufo_task_node_get_proc_node(UFO_TASK_NODE(task)));
  cmd_queue = ufo_gpu_node_get_cmd_queue(node);

  /*
  fprintf(stderr, "Entering %s.%d\n", __func__, __LINE__);
  fprintf(stderr, " output is : %d\n", priv->output);
  fprintf(stderr, " correction-mode is : %d\n", priv->mode);
  fprintf(stderr, " run length is : %u\n", priv->length);
  fprintf(stderr, " sino_width is : %u\n", sino_width);
  fprintf(stderr, " sino_projs is : %u\n", sino_projs);
  fprintf(stderr, " requisition->dims[0] is : %lu\n", requisition->dims[0]);
  fprintf(stderr, " requisition->dims[1] is : %lu\n", requisition->dims[1]);
  // */
  // Running the vertical running average of the sinogram
  kernel = priv->run_average_kernel;
  in_mem = ufo_buffer_get_device_array (inputs[0], cmd_queue);
  if ( OUTPUT_AVERAGE != priv->output ) { // Using an intermediate buffer/cl_mem :
    //    fprintf(stderr, "output to internal buffer\n");
    out_mem = priv->averaged;
  }
  else { // In this case we output this very particular result :
    //    fprintf(stderr, "output to task's output buffer\n");
    out_mem = ufo_buffer_get_device_array(output, cmd_queue);
  }

  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 0, sizeof(cl_mem), &in_mem));
  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 1, sizeof(cl_mem), &out_mem));
  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 2, sizeof(guint), &sino_projs));
  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 3, sizeof(guint), &priv->length));

  profiler = ufo_task_node_get_profiler(UFO_TASK_NODE(task));
  ufo_profiler_call(profiler, cmd_queue, kernel, 1, (requisition->dims) + 0, NULL); // one work-item per pixel

  if ( OUTPUT_AVERAGE == priv->output ) {
    // We are done : computed what was requested, we can return
    //    fprintf(stderr, "We are done with %s\n", __func__);
    return TRUE;
  }

  // Detecting/filtering out stripes in each averaged projection
  kernel = priv->smooth_proj_kernel;
  if ( OUTPUT_SMOOTHED != priv->output ) { // Using an intermediate buffer/cl_mem :
    out_mem = priv->smoothed;
  }
  else { // In this case we output this very particular result :
    out_mem = ufo_buffer_get_device_array(output, cmd_queue);
  }

  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 0, sizeof(cl_mem), &(priv->averaged)));
  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 1, sizeof(cl_mem), &out_mem));
  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 2, sizeof(guint), &sino_width));
  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 3, sizeof(guint), &priv->width));
  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 4, sizeof(gfloat), &priv->threshold));

  ufo_profiler_call(profiler, cmd_queue, kernel, 1, (requisition->dims) + 1, NULL); // one work-item per projection

  if ( OUTPUT_SMOOTHED == priv->output ) {
    // We are done : computed what was requested, we can return
    return TRUE;
  }

  // Get or apply the correction factor
  kernel = priv->correct_sino_kernel;
  out_mem = ufo_buffer_get_device_array(output, cmd_queue);
  // Some extra required parameters to the function :
  gfloat dir_scale = priv->max_correction;
  gfloat inv_scale = 1.0f / dir_scale;
  gfloat bot_clip = expf(-8.0f * priv->max_correction); // Only used for multiplicative mode of correction.

  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 0, sizeof(cl_mem), &in_mem));
  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 1, sizeof(cl_mem), &(priv->averaged)));
  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 2, sizeof(cl_mem), &(priv->smoothed)));
  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 3, sizeof(cl_mem), &out_mem));
  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 4, sizeof(gfloat), &bot_clip));
  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 5, sizeof(gfloat), &dir_scale));
  UFO_RESOURCES_CHECK_CLERR(clSetKernelArg(kernel, 6, sizeof(gfloat), &inv_scale));

  ufo_profiler_call(profiler, cmd_queue, kernel, 2, requisition->dims, NULL);

  return TRUE;
}


static void
ufo_destrip_faster_task_set_property (GObject *object,
                                        guint property_id,
                                        const GValue *value,
                                        GParamSpec *pspec)
{
  // OK ?
  UfoDestripFasterTaskPrivate *priv;
  priv = UFO_DESTRIP_FASTER_TASK_GET_PRIVATE (object);

  switch (property_id) {
  case PROP_THRESHOLD:
    priv->threshold = g_value_get_float(value);
    break;
  case PROP_MAX_CORRECTION:
    priv->max_correction = g_value_get_float(value);
    break;
  case PROP_WIDTH:
    priv->width = g_value_get_uint(value);
    break;
  case PROP_LENGTH:
    priv->length = g_value_get_uint(value);
    break;
  case PROP_OUTPUT:
    priv->output = g_value_get_enum(value);
    break;
  case PROP_MODE:
    priv->mode = g_value_get_enum(value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
ufo_destrip_faster_task_get_property (GObject *object,
                                        guint property_id,
                                        GValue *value,
                                        GParamSpec *pspec)
{
  UfoDestripFasterTaskPrivate *priv = UFO_DESTRIP_FASTER_TASK_GET_PRIVATE (object);

  // OK ?
  switch (property_id) {
  case PROP_THRESHOLD:
    g_value_set_float (value, priv->threshold);
    break;
  case PROP_MAX_CORRECTION:
    g_value_set_float (value, priv->max_correction);
    break;
  case PROP_WIDTH:
    g_value_set_uint (value, priv->width);
    break;
  case PROP_LENGTH:
    g_value_set_uint (value, priv->length);
    break;
  case PROP_OUTPUT:
    g_value_set_enum (value, priv->output);
    break;
  case PROP_MODE:
    g_value_set_enum (value, priv->mode);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
ufo_destrip_faster_task_finalize (GObject *object)
{
  UfoDestripFasterTaskPrivate *priv;
  priv = UFO_DESTRIP_FASTER_TASK_GET_PRIVATE (object);

  if ( priv->averaged ) {
    UFO_RESOURCES_CHECK_CLERR (clReleaseMemObject (priv->averaged));
    priv->averaged = NULL;
  }
  if ( priv->smoothed ) {
    UFO_RESOURCES_CHECK_CLERR (clReleaseMemObject (priv->smoothed));
    priv->smoothed = NULL;
  }

  if ( priv->run_average_kernel ) {
    UFO_RESOURCES_CHECK_CLERR (clReleaseKernel (priv->run_average_kernel));
    priv->run_average_kernel = NULL;
  }
  if ( priv->smooth_proj_kernel ) {
    UFO_RESOURCES_CHECK_CLERR (clReleaseKernel (priv->smooth_proj_kernel));
    priv->smooth_proj_kernel = NULL;
  }
  if ( priv->correct_sino_kernel ) {
    UFO_RESOURCES_CHECK_CLERR (clReleaseKernel (priv->correct_sino_kernel));
    priv->correct_sino_kernel = NULL;
  }

  if (priv->context) {
    UFO_RESOURCES_CHECK_CLERR (clReleaseContext (priv->context));
    priv->context = NULL;
  }

  // This one MUST remain the last line of this function :
  G_OBJECT_CLASS (ufo_destrip_faster_task_parent_class)->finalize (object);
}

static void
ufo_task_interface_init (UfoTaskIface *iface)
{
  // OK (default but seems not to need any extra) ?
  iface->setup = ufo_destrip_faster_task_setup;
  iface->get_num_inputs = ufo_destrip_faster_task_get_num_inputs;
  iface->get_num_dimensions = ufo_destrip_faster_task_get_num_dimensions;
  iface->get_mode = ufo_destrip_faster_task_get_mode;
  iface->get_requisition = ufo_destrip_faster_task_get_requisition;
  iface->process = ufo_destrip_faster_task_process;
}

static void
ufo_destrip_faster_task_class_init (UfoDestripFasterTaskClass *klass)
{
  // OK ?
  GObjectClass *oclass = G_OBJECT_CLASS (klass);

  oclass->set_property = ufo_destrip_faster_task_set_property;
  oclass->get_property = ufo_destrip_faster_task_get_property;
  oclass->finalize = ufo_destrip_faster_task_finalize;

  properties[PROP_THRESHOLD] =
    g_param_spec_float ("threshold",
                        "Threshold to detect stripes in angularly averaged sinogram",
                        "Any pixel in the averaged sinogram that is more this threshold away to the smoothed version (in unit of mad) is considered an outlier",
                        0.0, 20.0, 1.5,
                        G_PARAM_READWRITE);

  properties[PROP_MAX_CORRECTION] =
    g_param_spec_float ("max-correction",
                        "Maximum correction applied by the filter",
                        "The correction computed by the filter, comparing averaged and smoothed version of the sinogram is softly clipped to never be (or its opposite/inverse depending on correction mode) above this value",
                        0.0, 20.0, 2.0,
                        G_PARAM_READWRITE);

  properties[PROP_WIDTH] =
    g_param_spec_uint ("strip-width",
                       "The width of the stripes to be removed",
                       "The smoothing of the averaged sinogram is trying to detect any stripes which width is smaller than this parameter",
                       0, +64, +7,
                       G_PARAM_READWRITE);

  properties[PROP_LENGTH] =
    g_param_spec_uint ("average-length",
                       "Each projection will be averaged with this amount of projection before and after it",
                       "The number of projection to use before and after each projection to compute the local average on which is based the stripe detection (and removal)",
                       0, +32768, 500,
                       G_PARAM_READWRITE);

  properties[PROP_OUTPUT] =
    g_param_spec_enum ("output",
                       "Which of the computation should be send to output (\"destriped\", \"average\", \"smooth\", \"clipped-correction\", \"raw-correction\")",
                       "Which of the computation should be send to output (\"destriped\", \"average\", \"smooth\", \"clipped-correction\", \"raw-correction\"). The aim is to provide access to the intermediate results to ease understanding and tuning of the filter",
                       g_enum_register_static ("output", output_values),
                       OUTPUT_DESTRIPE, G_PARAM_READWRITE);

  properties[PROP_MODE] =
    g_param_spec_enum ("correction-mode",
                       "How to implement correction (\"additive\", \"multiplicative\")",
                       "How to implement correction (\"additive\", \"multiplicative\"). This enables the use of this filtering, repsectively, on either absorbance and transmitance images",
                       g_enum_register_static ("correction-mode", correction_mode_values),
                       CORRECT_ADDITIVE, G_PARAM_READWRITE);

  for (guint i = PROP_0 + 1; i < N_PROPERTIES; i++)
    g_object_class_install_property (oclass, i, properties[i]);

  //  g_type_class_add_private (oclass, sizeof(UfoDestripSinogramTaskPrivate));
}

static void
ufo_destrip_faster_task_init(UfoDestripFasterTask *self)
{
  UfoDestripFasterTaskPrivate *priv;
  self->priv = priv = UFO_DESTRIP_FASTER_TASK_GET_PRIVATE(self);

  // Initialising some values for the filter ?
  priv->context = NULL;
  priv->run_average_kernel = NULL;
  priv->smooth_proj_kernel = NULL;
  priv->correct_sino_kernel = NULL;
  priv->averaged = NULL;
  priv->smoothed = NULL;
  priv->threshold = 1.5f;
  priv->max_correction = 2.0f;
  priv->width = 7;
  priv->length = 500;
  priv->output = OUTPUT_DESTRIPE;
  priv->mode = CORRECT_ADDITIVE;
}
