/*
 * Computing the backprojection for irregular angular spacing.
 * This file is part of ufo-serge filter set.
 * Copyright (C) 2018 Serge Cohen
 *
 * This file is part of Ufo.
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Serge Cohen <serge.cohen@synchrotron-soleil.fr>
 */

#include <ufo/ufo-cl.h>

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "ufo-backproject-irregular-task.h"

// Trying to make it as close as possible to the (regular) backproject interface

typedef enum {
  MODE_NEAREST,
  MODE_TEXTURE
} Mode;

static GEnumValue mode_values[] = {
  { MODE_NEAREST, "MODE_NEAREST", "nearest" },
  { MODE_TEXTURE, "MODE_TEXTURE", "texture" },
  { 0, NULL, NULL}
};


struct _UfoBackprojectIrregularTaskPrivate {
  cl_context context;
  cl_kernel nearest_kernel;
  cl_kernel texture_kernel;
  cl_mem sin_lut;
  cl_mem cos_lut;
  cl_mem wedge_lut;
  gfloat *host_sin_lut;
  gfloat *host_cos_lut;
  gfloat *host_wedge_lut;
  gdouble axis_pos;
  gchar *angle_val_filename;  // filename of the file to parse to get angle values
  guint angle_val_size;   // size of the memory allocation for angle_values
  guint angle_val_num;    // number of values stored in angle_values (>= proj_size)
  gdouble *angle_values;  // array of measured angular position corresponding to each projection.
  gdouble angle_offset;   // will be added to angle_values to compute sin/cos luts
  gboolean luts_ready;    // luts have been computed and are up to date
  guint proj_size;    // total number of projections
  guint proj_offset;  // the index of first projection to backproject (aka start of projection ROI)
  guint proj_used;    // number of projections to back project (aka size of projection ROI)
  guint roi_x;
  guint roi_y;
  gint roi_width;
  gint roi_height;
  guint chuncking;
  Mode mode;
};

static void ufo_task_interface_init (UfoTaskIface *iface);
static void get_angles (UfoBackprojectIrregularTaskPrivate *priv, GError **error);
static void release_luts (UfoBackprojectIrregularTaskPrivate *priv, GError **error);
static void build_luts (UfoBackprojectIrregularTaskPrivate *priv, GError **error);

G_DEFINE_TYPE_WITH_CODE (UfoBackprojectIrregularTask, ufo_backproject_irregular_task, UFO_TYPE_TASK_NODE,
                         G_IMPLEMENT_INTERFACE (UFO_TYPE_TASK,
                                                ufo_task_interface_init)
			 G_ADD_PRIVATE(UfoBackprojectIrregularTask))

#define UFO_BACKPROJECT_IRREGULAR_TASK_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), UFO_TYPE_BACKPROJECT_IRREGULAR_TASK, UfoBackprojectIrregularTaskPrivate))

enum {
  PROP_0,
  PROP_ANGLE_FILENAME,
  PROP_NUM_PROJECTIONS,
  PROP_OFFSET_PROJECTIONS,
  PROP_AXIS_POSITION,
  PROP_ANGLE_OFFSET,
  PROP_ROI_X,
  PROP_ROI_Y,
  PROP_ROI_WIDTH,
  PROP_ROI_HEIGHT,
  PROP_MODE,       // Later on, for texture mode we might later propose clamping modes for projection value estimation outside sinogram area ?
  PROP_CHUNCKING,
  N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES] = { NULL, };

UfoNode *
ufo_backproject_irregular_task_new (void)
{
  // OK ?
  return UFO_NODE (g_object_new (UFO_TYPE_BACKPROJECT_IRREGULAR_TASK, NULL));
}

static void
ufo_backproject_irregular_task_setup (UfoTask *task,
                                      UfoResources *resources,
                                      GError **error)
{
  // OK ?
  UfoBackprojectIrregularTaskPrivate *priv;

  priv = UFO_BACKPROJECT_IRREGULAR_TASK_GET_PRIVATE (task);

  // Building up the OpenCL kernels :
  priv->context = ufo_resources_get_context (resources);
  priv->nearest_kernel = ufo_resources_get_kernel (resources, "backproject-irregular.cl", "backproject_nearest", NULL, error);
  priv->texture_kernel = ufo_resources_get_kernel (resources, "backproject-irregular.cl", "backproject_tex", NULL, error);

  UFO_RESOURCES_CHECK_CLERR (clRetainContext (priv->context));

  if (priv->nearest_kernel != NULL)
    UFO_RESOURCES_CHECK_CLERR (clRetainKernel (priv->nearest_kernel));

  if (priv->texture_kernel != NULL)
    UFO_RESOURCES_CHECK_CLERR (clRetainKernel (priv->texture_kernel));

  // Code at some point to read the angle file (is this the proper place to call it ?)
  get_angles(priv, error);
}

static void
ufo_backproject_irregular_task_get_requisition (UfoTask *task,
                                                UfoBuffer **inputs,
                                                UfoRequisition *requisition,
                                                GError **error)
{
  // Kind of OK (at least for an initial version)
  // Later should manage to be smarter on pro_offset and proj_number
  UfoBackprojectIrregularTaskPrivate *priv;
  UfoRequisition in_req;

  priv = UFO_BACKPROJECT_IRREGULAR_TASK_GET_PRIVATE (task);
  ufo_buffer_get_requisition (inputs[0], &in_req);

  /* Taking care of the requisition itself */
  requisition->n_dims = 2;
  requisition->dims[0] = (priv->roi_width == 0) ? in_req.dims[0] : (gsize) priv->roi_width;
  requisition->dims[1] = (priv->roi_height == 0) ? in_req.dims[0] : (gsize) priv->roi_height;

  /* Taking the opportunity to update other parameters, if so required */
  if ( ! priv->luts_ready ) {
    GError *error = NULL;
    build_luts(priv, &error);
  }

  // Still have to take care of the values (sensible defaults if not provided) of :
  priv->proj_size = in_req.dims[1]; // Total number of projections availbale in sinogram
  // priv->proj_offset defaulted to 0
  if ( (priv->proj_offset + priv->proj_used ) > priv->proj_size ) {
    g_error("Total number of projection lower than that is requested to perform the reconstruction :\n"
            "%u (sinogram heigh) < (%u (offset) + %u (number) )", priv->proj_size, priv->proj_offset, priv->proj_used);
  }
  if ( (priv->proj_offset + priv->proj_used ) > priv->angle_val_num ) {
    g_error("Total number of projection angles lower than that is requested to perform the reconstruction :\n"
            "%u (number of angle) < (%u (offset) + %u (number) )", priv->angle_val_num, priv->proj_offset, priv->proj_used);
  }
  if ( 0 == priv->proj_used ) { // defaulting to proj_size - proj_offset
    priv->proj_used = priv->proj_size - priv->proj_offset;
  }
}

static guint
ufo_backproject_irregular_task_get_num_inputs (UfoTask *task)
{
  // OK
  return 1;
}

static guint
ufo_backproject_irregular_task_get_num_dimensions (UfoTask *task,
                                                   guint input)
{
  // OK
  return 2;
}

static UfoTaskMode
ufo_backproject_irregular_task_get_mode (UfoTask *task)
{
  // OK (default but seems not to need any extra) ?
  return UFO_TASK_MODE_PROCESSOR | UFO_TASK_MODE_GPU;
}

static gboolean
ufo_backproject_irregular_task_process (UfoTask *task,
                                        UfoBuffer **inputs,
                                        UfoBuffer *output,
                                        UfoRequisition *requisition)
{
  // Should be OK ?
  UfoBackprojectIrregularTaskPrivate *priv;
  UfoGpuNode *node;
  UfoProfiler *profiler;
  cl_command_queue cmd_queue;
  cl_mem in_mem;
  cl_mem out_mem;
  cl_kernel kernel;
  gfloat axis_pos;
  UfoRequisition in_req;
  guint proj_width;
  gchar proj_init;
  guint proj_end;
  guint num_chunck;
  guint chunck_size;
  guint chunck_end;

  priv = UFO_BACKPROJECT_IRREGULAR_TASK_GET_PRIVATE (task);
  node = UFO_GPU_NODE (ufo_task_node_get_proc_node (UFO_TASK_NODE (task)));
  cmd_queue = ufo_gpu_node_get_cmd_queue (node);
  out_mem = ufo_buffer_get_device_array (output, cmd_queue);
  ufo_buffer_get_requisition (inputs[0], &in_req);

  if (priv->mode == MODE_TEXTURE) {
    in_mem = ufo_buffer_get_device_image (inputs[0], cmd_queue);
    kernel = priv->texture_kernel;
  }
  else {
    in_mem = ufo_buffer_get_device_array (inputs[0], cmd_queue);
    kernel = priv->nearest_kernel;
  }
  /* Providing the width of the sinogram, required for /nearest/ reconstruction within a ROI */
  proj_width = in_req.dims[0];

  /* Guess axis position if they are not provided by the user. */
  if (priv->axis_pos <= 0.0) {
    axis_pos = (gfloat) ((gfloat) proj_width) / 2.0f;
  }
  else {
    axis_pos = priv->axis_pos;
  }

  // Getting ready to perform computation by chuncks :
  proj_init = 1;
  num_chunck = (0 != priv->chuncking) ? priv->chuncking : 1;
  proj_end = priv->proj_offset + priv->proj_used;
  chunck_size = priv->proj_used / num_chunck;

  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 0, sizeof (cl_mem), &in_mem));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 1, sizeof (cl_mem), &out_mem));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 2, sizeof (cl_mem), &priv->sin_lut));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 3, sizeof (cl_mem), &priv->cos_lut));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 4, sizeof (cl_mem), &priv->wedge_lut));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 5, sizeof (guint), &priv->roi_x));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 6, sizeof (guint), &priv->roi_y));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 10, sizeof (guint), &proj_width));
  UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 11, sizeof (gfloat), &axis_pos));

  for ( guint chunck_start=priv->proj_offset ; chunck_start < proj_end; chunck_start += chunck_size ) {
    chunck_end = chunck_start + chunck_size;
    chunck_end = (chunck_end < proj_end) ? chunck_end : proj_end;

    UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 7, sizeof (gchar), &proj_init));
    UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 8, sizeof (guint), &chunck_start));
    UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (kernel, 9, sizeof (guint), &chunck_end));
#warning To test chuncking...
    profiler = ufo_task_node_get_profiler (UFO_TASK_NODE (task));
    if ( priv->chuncking ) {
      ufo_profiler_call_blocking(profiler, cmd_queue, kernel, 2, requisition->dims, NULL);
      printf("*");
      fflush(stdout);
    }
    else {
      ufo_profiler_call(profiler, cmd_queue, kernel, 2, requisition->dims, NULL);
    }
    proj_init = 0; // We are sure it is false for the next now
  }

  return TRUE;
}


static void
ufo_backproject_irregular_task_set_property (GObject *object,
                                             guint property_id,
                                             const GValue *value,
                                             GParamSpec *pspec)
{
  // OK ?
  UfoBackprojectIrregularTaskPrivate *priv = UFO_BACKPROJECT_IRREGULAR_TASK_GET_PRIVATE (object);

  switch (property_id) {
  case PROP_ANGLE_FILENAME:
    g_free (priv->angle_val_filename);
    priv->angle_val_filename = g_value_dup_string (value);
    priv->luts_ready = FALSE;
    break;
  case PROP_NUM_PROJECTIONS:
    priv->proj_used = g_value_get_uint(value);
    break;
  case PROP_OFFSET_PROJECTIONS:
    priv->proj_offset = g_value_get_uint(value);
    break;
  case PROP_AXIS_POSITION:
    priv->axis_pos = g_value_get_double (value);
    break;
  case PROP_ANGLE_OFFSET:
    priv->angle_offset = g_value_get_double (value);
    priv->luts_ready = FALSE;
    break;
  case PROP_ROI_X:
    priv->roi_x = g_value_get_uint (value);
    break;
  case PROP_ROI_Y:
    priv->roi_y = g_value_get_uint (value);
    break;
  case PROP_ROI_WIDTH:
    priv->roi_width = g_value_get_uint (value);
    break;
  case PROP_ROI_HEIGHT:
    priv->roi_height = g_value_get_uint (value);
    break;
  case PROP_MODE:
    priv->mode = g_value_get_enum (value);
    break;
  case PROP_CHUNCKING:
    priv->chuncking = g_value_get_uint (value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
ufo_backproject_irregular_task_get_property (GObject *object,
                                             guint property_id,
                                             GValue *value,
                                             GParamSpec *pspec)
{
  // OK ?
  UfoBackprojectIrregularTaskPrivate *priv = UFO_BACKPROJECT_IRREGULAR_TASK_GET_PRIVATE (object);

  switch (property_id) {
  case PROP_ANGLE_FILENAME:
    g_value_set_string(value, priv->angle_val_filename);
    break;
  case PROP_NUM_PROJECTIONS:
    g_value_set_uint(value, priv->proj_used);
    break;
  case PROP_OFFSET_PROJECTIONS:
    g_value_set_uint(value, priv->proj_offset);
    break;
  case PROP_AXIS_POSITION:
    g_value_set_double (value, priv->axis_pos);
    break;
  case PROP_ANGLE_OFFSET:
    g_value_set_double (value, priv->angle_offset);
    break;
  case PROP_ROI_X:
    g_value_set_uint (value, priv->roi_x);
    break;
  case PROP_ROI_Y:
    g_value_set_uint (value, priv->roi_y);
    break;
  case PROP_ROI_WIDTH:
    g_value_set_uint (value, priv->roi_width);
    break;
  case PROP_ROI_HEIGHT:
    g_value_set_uint (value, priv->roi_height);
    break;
  case PROP_MODE:
    g_value_set_enum (value, priv->mode);
    break;
  case PROP_CHUNCKING:
    g_value_set_uint (value, priv->chuncking);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
ufo_backproject_irregular_task_finalize (GObject *object)
{
  // OK ?
  UfoBackprojectIrregularTaskPrivate *priv;
  priv = UFO_BACKPROJECT_IRREGULAR_TASK_GET_PRIVATE(object);
  GError *error = NULL;

  release_luts(priv, &error);
  g_free(priv->angle_values);

  if (priv->nearest_kernel) {
    UFO_RESOURCES_CHECK_CLERR (clReleaseKernel (priv->nearest_kernel));
    priv->nearest_kernel = NULL;
  }

  if (priv->texture_kernel) {
    UFO_RESOURCES_CHECK_CLERR (clReleaseKernel (priv->texture_kernel));
    priv->texture_kernel = NULL;
  }

  if (priv->context) {
    UFO_RESOURCES_CHECK_CLERR (clReleaseContext (priv->context));
    priv->context = NULL;
  }

  // Next is the very last line of the function :
  G_OBJECT_CLASS (ufo_backproject_irregular_task_parent_class)->finalize (object);
}

static void
ufo_task_interface_init (UfoTaskIface *iface)
{
  // OK (default)
  iface->setup = ufo_backproject_irregular_task_setup;
  iface->get_num_inputs = ufo_backproject_irregular_task_get_num_inputs;
  iface->get_num_dimensions = ufo_backproject_irregular_task_get_num_dimensions;
  iface->get_mode = ufo_backproject_irregular_task_get_mode;
  iface->get_requisition = ufo_backproject_irregular_task_get_requisition;
  iface->process = ufo_backproject_irregular_task_process;
}

static void
ufo_backproject_irregular_task_class_init (UfoBackprojectIrregularTaskClass *klass)
{
  // OK ?
  GObjectClass *oclass = G_OBJECT_CLASS (klass);
  const gfloat limit = (gfloat) (4.0 * G_PI);

  oclass->set_property = ufo_backproject_irregular_task_set_property;
  oclass->get_property = ufo_backproject_irregular_task_get_property;
  oclass->finalize = ufo_backproject_irregular_task_finalize;


  properties[PROP_ANGLE_FILENAME] =
    g_param_spec_string ("angles-filename",
                         "Path to the file containing the list of projection angles",
                         "Path to the file containing the list of projection angles. This file should contain one angle value per projection, written in ASCII, base 10 and in radian.",
                         "",
                         G_PARAM_READWRITE);

  properties[PROP_NUM_PROJECTIONS] =
    g_param_spec_uint ("projection-num",
                       "Number of projections to backproject",
                       "Enabling to reconstruct based on a sub-part of the sinogram, to be used together with projection-offset",
                       0, +32768, 0,
                       G_PARAM_READWRITE);
  // A value of 0 means /as many as possible, provided the offset/

  properties[PROP_OFFSET_PROJECTIONS] =
    g_param_spec_uint ("projection-offset",
                       "The index of the first projection ot be backprojected",
                       "Enabling to reconstruct based on a sub-part of the sinogram, to be used together with projection-num",
                       0, +32768, 0,
                       G_PARAM_READWRITE);

  properties[PROP_AXIS_POSITION] =
    g_param_spec_double ("axis-pos",
                         "Position of rotation axis, in fractional pixel index",
                         "Position of rotation axis, in fractional pixel index",
                         -1.0, +8192.0, 0.0,
                         G_PARAM_READWRITE);

  properties[PROP_ANGLE_OFFSET] =
    g_param_spec_double ("angle-offset",
                         "Angle offset in radians",
                         "Angle offset in radians, added to the values read from the angle-file",
                         0.0, +limit, 0.0,
                         G_PARAM_READWRITE);

  properties[PROP_ROI_X] =
    g_param_spec_uint ("roi-x",
                       "X coordinate of region of interest",
                       "X coordinate of region of interest",
                       0, G_MAXUINT, 0,
                       G_PARAM_READWRITE);

  properties[PROP_ROI_Y] =
    g_param_spec_uint ("roi-y",
                       "Y coordinate of region of interest",
                       "Y coordinate of region of interest",
                       0, G_MAXUINT, 0,
                       G_PARAM_READWRITE);

  properties[PROP_ROI_WIDTH] =
    g_param_spec_uint ("roi-width",
                       "Width of region of interest",
                       "Width of region of interest",
                       0, G_MAXUINT, 0,
                       G_PARAM_READWRITE);

  properties[PROP_ROI_HEIGHT] =
    g_param_spec_uint ("roi-height",
                       "Height of region of interest",
                       "Height of region of interest",
                       0, G_MAXUINT, 0,
                       G_PARAM_READWRITE);

  properties[PROP_MODE] =
    g_param_spec_enum ("mode",
                       "Backprojection mode (\"nearest\", \"texture\")",
                       "Backprojection mode (\"nearest\", \"texture\")",
                       g_enum_register_static ("mode", mode_values),
                       MODE_TEXTURE, G_PARAM_READWRITE);

  properties[PROP_CHUNCKING] =
    g_param_spec_uint ("chuncking",
                       "Chuncking the backprojection of large regions to avoid hitting the GPU timeout (watchdog). 0 or 1 for no chuncking, otherwise kernel is splitting the backprojection in 'chuncking' calls",
                       "Chuncking the backprojection of large regions to avoid hitting the GPU timeout (watchdog). 0 or 1 for no chuncking, otherwise kernel is splitting the backprojection in 'chuncking' calls",
                       0, 256, 0,
                       G_PARAM_READWRITE);

  for (guint i = PROP_0 + 1; i < N_PROPERTIES; i++)
    g_object_class_install_property (oclass, i, properties[i]);

#warning The backproject task is installing an node_class->equal specific function, required here ?

  //  g_type_class_add_private (oclass, sizeof(UfoBackprojectIrregularTaskPrivate));
}

static void
ufo_backproject_irregular_task_init(UfoBackprojectIrregularTask *self)
{
  // OK ?
  UfoBackprojectIrregularTaskPrivate *priv;
  self->priv = priv = UFO_BACKPROJECT_IRREGULAR_TASK_GET_PRIVATE(self);

  priv->context = NULL;
  priv->nearest_kernel = NULL;
  priv->texture_kernel = NULL;
  priv->sin_lut = NULL;
  priv->cos_lut = NULL;
  priv->wedge_lut = NULL;
  priv->host_sin_lut = NULL;
  priv->host_cos_lut = NULL;
  priv->host_wedge_lut = NULL;
  priv->axis_pos = -1.0;
  priv->angle_val_filename = NULL;
  priv->angle_val_size = 0;
  priv->angle_val_num = 0;
  priv->angle_values = NULL;
  priv->angle_offset = 0.0;
  priv->luts_ready = FALSE;
  priv->proj_size = 0;
  priv->proj_offset = 0;
  priv->proj_used = 0;
  priv->roi_x = 0;
  priv->roi_y = 0;
  priv->roi_width = 0;
  priv->roi_height = 0;
  priv->chuncking = 1;
  priv->mode = MODE_TEXTURE;
}

/* Functions specific to the this very particular filter */

static void
get_angles (UfoBackprojectIrregularTaskPrivate *priv,
            GError **error)
{
  // OK ?
  if ( NULL == priv->angle_val_filename ) {
    // This is an error, no file to read, return immediately, just after generating an error
    // This is the proper function (g_set_error) but have to change parameters to fit the need !
    g_set_error (error, UFO_TASK_ERROR, UFO_TASK_ERROR_SETUP,
                 "You have to provide a non-empty filename for the projection angle values");
    return;
  }

  if ( priv->angle_values) {
    g_free(priv->angle_values);
    priv->angle_values = NULL;
  }

  priv->angle_val_size = 8192;
  priv->angle_val_num = 0;
  priv->angle_values = g_realloc(NULL, priv->angle_val_size * sizeof(gdouble));

  // Opening the file and reading one number at a time :
  FILE *fp = fopen(priv->angle_val_filename, "r");
  if ( NULL == fp ) {
    g_set_error (error, G_IO_ERROR, g_io_error_from_errno (errno),
                 "Cannot access backproject-irregular angles-filename : `%s': %s.", priv->angle_val_filename, strerror (errno));
    return;
  }

  double current_angle;
  while ( EOF != fscanf(fp, "%lf", &current_angle) ) {
    if ( priv->angle_val_num == priv->angle_val_size ) {
      // Need to grow the buffer of angular values
      priv->angle_val_size += 8192;
      priv->angle_values = g_realloc(priv->angle_values, priv->angle_val_size);
    }
    priv->angle_values[priv->angle_val_num] = current_angle;
    ++(priv->angle_val_num);
  }
  fprintf(stderr, "Number of angle values read from %s is %d\n", priv->angle_val_filename, priv->angle_val_num);
  fclose(fp);

  // Signaling that the luts have to be computed :
  priv->luts_ready = FALSE;
}

static void
release_luts (UfoBackprojectIrregularTaskPrivate *priv,
              GError **error)
{
  // OK ?
  // Not forgetting this one, hence place it first :
  priv->luts_ready = FALSE;

  // Releasing memory (both main and OpenCL)
  if (priv->sin_lut) {
    UFO_RESOURCES_CHECK_CLERR (clReleaseMemObject (priv->sin_lut));
    priv->sin_lut = NULL;
  }
  if (priv->cos_lut) {
    UFO_RESOURCES_CHECK_CLERR (clReleaseMemObject (priv->cos_lut));
    priv->cos_lut = NULL;
  }
  if (priv->wedge_lut) {
    UFO_RESOURCES_CHECK_CLERR (clReleaseMemObject (priv->wedge_lut));
    priv->wedge_lut = NULL;
  }

  if ( priv->host_sin_lut ) {
    g_free(priv->host_sin_lut);
    priv->host_sin_lut = NULL;
  }
  if ( priv->host_cos_lut ) {
    g_free(priv->host_cos_lut);
    priv->host_cos_lut = NULL;
  }
  if ( priv->host_wedge_lut ) {
    g_free(priv->host_wedge_lut);
    priv->host_wedge_lut = NULL;
  }
}

static void
build_luts (UfoBackprojectIrregularTaskPrivate *priv,
            GError **error)
{
  // OK ?
  if ( priv->luts_ready ) // Shortcut : nothing to be done, all ready.
    return;

  if ( NULL == priv->angle_values ) { // In error : we cannot compute the luts if we do not have the angular values.
    g_set_error(error, UFO_TASK_GRAPH_ERROR, UFO_TASK_GRAPH_ERROR_BAD_INPUTS,
                "Cannot build LUTS of backproject-irregular while angular values were not read in (supposedly file '%s')", priv->angle_val_filename);
    return;
  }

  release_luts(priv, error);
  if ( NULL != *error ) { // Propagating the error, without doing anything (maybe some better way exists in glib ?)
    return;
  }
  // Trying to get best possible alignement by allocating each LUT separately (host buffers):
  guint lut_size = priv->angle_val_num;
  priv->host_sin_lut = g_malloc0_n(sizeof(gfloat), lut_size);
  priv->host_cos_lut = g_malloc0_n(sizeof(gfloat), lut_size);
  priv->host_wedge_lut = g_malloc0_n(sizeof(gfloat), lut_size);
  for ( guint i=0; lut_size != i; ++i ) {
    gdouble current_angle = priv->angle_values[i] + priv->angle_offset;
    priv->host_sin_lut[i] = (gfloat)(sin(current_angle));
    priv->host_cos_lut[i] = (gfloat)(cos(current_angle));
    priv->host_wedge_lut[i] = (gfloat)((i != (lut_size-1)) ?
                                       (priv->angle_values[i+1] - priv->angle_values[i]) :
                                       (priv->angle_values[i] - priv->angle_values[i-1]));
  }

  // Now constructing the cl_mem buffers :
  cl_int errcode;
  size_t lut_size_B = lut_size * sizeof(float);
  priv->sin_lut = clCreateBuffer(priv->context,
                                 CL_MEM_COPY_HOST_PTR | CL_MEM_READ_ONLY,
                                 lut_size_B, priv->host_sin_lut,
                                 &errcode);
  UFO_RESOURCES_CHECK_CLERR (errcode);
  priv->cos_lut = clCreateBuffer(priv->context,
                                 CL_MEM_COPY_HOST_PTR | CL_MEM_READ_ONLY,
                                 lut_size_B, priv->host_cos_lut,
                                 &errcode);
  UFO_RESOURCES_CHECK_CLERR (errcode);
  priv->wedge_lut = clCreateBuffer(priv->context,
                                   CL_MEM_COPY_HOST_PTR | CL_MEM_READ_ONLY,
                                   lut_size_B, priv->host_wedge_lut,
                                   &errcode);
  UFO_RESOURCES_CHECK_CLERR (errcode);

  priv->luts_ready = TRUE;
  return;
}
