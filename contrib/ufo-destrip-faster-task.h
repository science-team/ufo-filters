/*
 * Removing vertical stripes in sinogram to reduce ring artefacts.
 * This file is part of ufo-serge filter set.
 * Copyright (C) 2018 Serge Cohen
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Serge Cohen <serge.cohen@synchrotron-soleil.fr>
 */

#ifndef __UFO_DESTRIP_FASTER_TASK_H
#define __UFO_DESTRIP_FASTER_TASK_H

#include <ufo/ufo.h>

G_BEGIN_DECLS

#define UFO_TYPE_DESTRIP_FASTER_TASK             (ufo_destrip_faster_task_get_type())
#define UFO_DESTRIP_FASTER_TASK(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj), UFO_TYPE_DESTRIP_FASTER_TASK, UfoDestripFasterTask))
#define UFO_IS_DESTRIP_FASTER_TASK(obj)          (G_TYPE_CHECK_INSTANCE_TYPE((obj), UFO_TYPE_DESTRIP_FASTER_TASK))
#define UFO_DESTRIP_FASTER_TASK_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST((klass), UFO_TYPE_DESTRIP_FASTER_TASK, UfoDestripFasterTaskClass))
#define UFO_IS_DESTRIP_FASTER_TASK_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE((klass), UFO_TYPE_DESTRIP_FASTER_TASK))
#define UFO_DESTRIP_FASTER_TASK_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS((obj), UFO_TYPE_DESTRIP_FASTER_TASK, UfoDestripFasterTaskClass))

typedef struct _UfoDestripFasterTask           UfoDestripFasterTask;
typedef struct _UfoDestripFasterTaskClass      UfoDestripFasterTaskClass;
typedef struct _UfoDestripFasterTaskPrivate    UfoDestripFasterTaskPrivate;

struct _UfoDestripFasterTask {
  UfoTaskNode parent_instance;

  UfoDestripFasterTaskPrivate *priv;
};

struct _UfoDestripFasterTaskClass {
  UfoTaskNodeClass parent_class;
};

UfoNode  *ufo_destrip_faster_task_new       (void);
GType     ufo_destrip_faster_task_get_type  (void);

G_END_DECLS

#endif
