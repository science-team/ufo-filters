
/*
 * Binning voxels according to various available kernels/functions.
 * This file is part of ufo-serge filter set.
 * Copyright (C) 2018 Serge Cohen
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Serge Cohen <serge.cohen@synchrotron-soleil.fr>
 */

#ifndef __UFO_BIN_VOXELS_TASK_H
#define __UFO_BIN_VOXELS_TASK_H

#include <ufo/ufo.h>

G_BEGIN_DECLS

#define UFO_TYPE_BIN_VOXELS_TASK             (ufo_bin_voxels_task_get_type())
#define UFO_BIN_VOXELS_TASK(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj), UFO_TYPE_BIN_VOXELS_TASK, UfoBinVoxelsTask))
#define UFO_IS_BIN_VOXELS_TASK(obj)          (G_TYPE_CHECK_INSTANCE_TYPE((obj), UFO_TYPE_BIN_VOXELS_TASK))
#define UFO_BIN_VOXELS_TASK_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST((klass), UFO_TYPE_BIN_VOXELS_TASK, UfoBinVoxelsTaskClass))
#define UFO_IS_BIN_VOXELS_TASK_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE((klass), UFO_TYPE_BIN_VOXELS_TASK))
#define UFO_BIN_VOXELS_TASK_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS((obj), UFO_TYPE_BIN_VOXELS_TASK, UfoBinVoxelsTaskClass))

typedef struct _UfoBinVoxelsTask           UfoBinVoxelsTask;
typedef struct _UfoBinVoxelsTaskClass      UfoBinVoxelsTaskClass;
typedef struct _UfoBinVoxelsTaskPrivate    UfoBinVoxelsTaskPrivate;

struct _UfoBinVoxelsTask {
  UfoTaskNode parent_instance;

  UfoBinVoxelsTaskPrivate *priv;
};

struct _UfoBinVoxelsTaskClass {
  UfoTaskNodeClass parent_class;
};

UfoNode  *ufo_bin_voxels_task_new       (void);
GType     ufo_bin_voxels_task_get_type  (void);

G_END_DECLS

#endif
