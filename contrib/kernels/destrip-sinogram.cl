/*
 * Removing vertical stripes in sinogram to reduce ring artefacts.
 * This file is part of ufo-serge filter set.
 * Copyright (C) 2018 Serge Cohen
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Serge Cohen <serge.cohen@synchrotron-soleil.fr>
 */

/*
 * This program contains the different kernels necessary to perform the
 * computation :
 *
 * 1. The kernel to perform the running average of the projections of the
 *    sinogram
 *
 * 2. The kernel to computed the smoothed version of the running average
 *    This kernel requires a compile time parameter (the size of the queue
 *    required to compute median and mad).
 *
 * 3. The kernel to apply the correcting factor found (in 1. and 2.) to the sinogram
 *
 */

// The best is to define MED_LENGTH at compile time to be just ajusted to
// the smoothing filter needs
#ifndef MED_LENGTH
#define MED_LENGTH 31
#endif

// Telling the last kernel to compute either the corrected sinogram
// or "just" the correction factor
// Current mapping is :
// 0 -> destriped
// 1 -> destriped-raw
// 2 -> average
// 3 -> smooth
// 4 -> clipped-correction
// 5 -> raw-correction
#ifndef SINO_OUTPUT
#define SINO_OUTPUT 0
#endif

// Which mode of correction should be applied
// 0 -> additive
// 1 -> multiplicative
#ifndef COR_MODE
#define COR_MODE 0
#endif

#define MED_BUFF_SIZE ((MED_LENGTH+1)<<1)

typedef float median_buffer[MED_BUFF_SIZE];

void sort(median_buffer io_buff, const unsigned int buff_size);

// One workitem per "column" of the sinogram (that is one pixel of the
// detector, over all angular range)
kernel void
run_average(global float *in_sino,
            global float *averaged_sino,
            const unsigned int num_projections,
            const unsigned int run_length)
{
  const size_t idx = get_global_id(0);
  const size_t width = get_global_size(0);
  const size_t first_length = min(num_projections, run_length+1);

  size_t idproj; // the index of the line currently being computed
  float current_length; // the number of pixels summed into the currently averaged one
  float current_sum;

  // Proceeding by recursion for the summations :
  // 1. Initialise the first line (for loop)
  current_sum = 0.0f;
  current_length = (float)(first_length);
  for ( size_t i=0; i != first_length; ++i ) {
    current_sum += in_sino[idx + i*width];
  }

  // 2. Fill in line n+1 as an update of line n
  idproj = 0;
  while ( idproj != num_projections ) {
    if ( num_projections > (idproj + run_length) ) { // Adding an extra element from the 'bottom'
      current_sum += in_sino[idx + (idproj + run_length) * width];
      current_length += 1.0f;
    }
    if ( idproj > run_length) { // Removing an element from the 'top'
      current_sum -= in_sino[idx + (idproj - run_length - 1) * width];
      current_length -= 1.0f;
    }
    averaged_sino[idx + idproj*width] = current_sum/current_length;
    ++idproj;
  }
}

// A function to sort a median_buffer using bubble sort :
// Later it might be even faster to /just/ compute the median
// (we do not need left and right parts to be internally sorted).
void
sort(median_buffer io_buff,
     const unsigned int buff_size)
{
  float swapper;

  for ( size_t i=buff_size; 1 != i; --i ) {
    for ( size_t j=1; i != j; ++j) {
      swapper = io_buff[j];
      io_buff[j]   = fmax(swapper, io_buff[j-1]);
      io_buff[j-1] = fmin(swapper, io_buff[j-1]);
    }
  }
}


// One wokritem per "row" of the sinogram, that is taking care of one
// angle and all of its pixels
kernel void
smooth_proj(global float *averaged_sino,
            global float *smoothed_sino,
            const unsigned int proj_width,
            const unsigned int stripe_width,
            const float threshold)
{
  const size_t base_index = proj_width * get_global_id(0);
  median_buffer med_vals; // Buffer used to compute the median
  median_buffer mad_vals; // Buffer used to compute the MAD

  // Register float use through out the computation :
  float swapper;
  float median;
  float mad;

  // Initialising the loop :
  size_t id_px = 0;
  size_t cur_le = MED_LENGTH;

  for ( size_t i=0; MED_LENGTH != i; ++i ) { // Loading data :
    med_vals[i] = averaged_sino[base_index+i];
  }
  sort(med_vals, MED_LENGTH);

  // Start looping, untill we are done with the projection/sinogram line :
  while ( proj_width != id_px ) {
    // Adding a new element if available
    if ( (id_px + MED_LENGTH) < proj_width ) {
      med_vals[cur_le] = averaged_sino[base_index + id_px + MED_LENGTH];
      for ( size_t i=cur_le; 0 != i; --i ) {
        swapper = med_vals[i];
        med_vals[i]   = fmax(swapper, med_vals[i-1]);
        med_vals[i-1] = fmin(swapper, med_vals[i-1]);
      }
      ++cur_le;
    }

    // Computing the median (odd/even length related)
    median = ( cur_le & 0x1 ) ? med_vals[cur_le >> 1] : (0.5f * (med_vals[(cur_le >> 1)-1] + med_vals[cur_le >> 1]));

    // Preparing MAD computation
    for ( size_t i=0; cur_le != i; ++i ) {
      mad_vals[i] = fabs(med_vals[i] - median); // Might be faster to later decompose in two different loops ?
    }
    // Sorting MAD
    sort(mad_vals, cur_le);

    // Computing MAD
    mad = ( cur_le & 0x1 ) ? mad_vals[cur_le >> 1] : (0.5f * (mad_vals[(cur_le >> 1)-1] + mad_vals[cur_le >> 1]));

    // Filtering (or not) the current pixel
    smoothed_sino[base_index + id_px] = (fabs(averaged_sino[base_index + id_px] - median) < (threshold * mad)) ?
      averaged_sino[base_index + id_px] : median;

    // Removing the 'oldest' element if appropriate
    if ( id_px > MED_LENGTH ) {
      swapper = averaged_sino[base_index + id_px - MED_LENGTH];
      bool contract = false;

      --cur_le;
      for ( size_t i=0; cur_le != i; ++i) {
        contract = (swapper ==  med_vals[i]) ? true : contract;
        med_vals[i] = (contract) ? med_vals[i+1] : med_vals[i];
      }
    }
    // Incrementing id_px
    ++id_px;
  }
}

/*  *** THIS ONE IS TO BE FIXED *** */
//
// -> depends on the scale (after log/absorbance computation or in lin/transmittance scale)
// -> also have to consider that it would be nice to have two parameters, one for saturation
//    level, the other being the /hardness/ of the saturation.
//
// Each workitem correspond to one pixel of the sinogram, that is a couple
// made of a angular value and a detector pixel.
// NB (for multiplicative mode only): corr_ratio may be negative in extreme cases
// (eg. in place where bright-field is very low)
// In this case we have to make sure not to try to compute the log of it, this is the purpose
// of the bot_clip parameter (a positive number "close to zero"), and we use as scaling
// correction the lowest possible one (the clipped, low-saturation value).
//
// Currently applying soft thresholding through tanh which is harder than atan (which is much
// too soft).
//
// NB : when running this kernel, SINO_OUTPUT is one of 0 (destriped), 1 (destriped-raw),
//      4 (clipper-correction), 5 (raw-correction). The other two values (2 - average and
//      3 - smooth) do not need the execution of this kernel.
kernel void
correct_sino(global float *in_sino,
             global float *averaged_sino,
             global float *smoothed_sino,
             global float *corrected_sino,
             const float bot_clip,
             const float dir_scale,
             const float inv_scale)
{
  const size_t idx = get_global_id(0);
  const size_t idy = get_global_id(1);
  const size_t dimx = get_global_size(0);
  const size_t index = idx + idy * dimx;
#if 1 == COR_MODE // Using multiplicative correction :
  float corr_val = smoothed_sino[index] / averaged_sino[index];
#else // Defaulting to use additive correction :
  float corr_val = smoothed_sino[index] - averaged_sino[index];
#endif // COR_MODE

#if 5 == SINO_OUTPUT // raw-correction
  corrected_sino[index] = corr_val;
#else

  // If we have to perform clipping :
#if 1 != SINO_OUTPUT
  // Clipping is not performed the same way depending on the correction mode :
#if 1 == COR_MODE // Multiplicative correction
  corr_val = (corr_val > bot_clip) ? exp(dir_scale * tanh(inv_scale * log(corr_val))) : exp(-dir_scale);
#else // Defaulting to additive mode
  corr_val = dir_scale * tanh(inv_scale * corr_val);
#endif // COR_MODE
#endif // 1 != SINO_OUTPUT

#if 4 == SINO_OUTPUT // returning the clipped correction :
    corrected_sino[index] = corr_val;
#else

#if 1 == COR_MODE // Multiplicative correction
  corrected_sino[index] = corr_val * in_sino[index];
#else // Defaulting to additive mode
  corrected_sino[index] = corr_val + in_sino[index];
#endif // COR_MODE

#endif // 4 == SINO_OUTPUT (clipper correction)
#endif // 5 == SINO_OUTPUT (raw correction)
}
