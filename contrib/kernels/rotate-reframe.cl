/*
 * Rotating and reframing an image
 * This file is part of ufo-serge filter set.
 * Copyright (C) 2021 Serge Cohen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Serge Cohen <serge.cohen@ipanema-remote.fr>
 */

/*
 * The rotation is performed using bilinear interpolation
 * clampling to edgewhen outside of input image
 */


/* o_im The output image
 * i_im The input image
 * i_sampler The image sampler to use to get value of the new pixel
 * i_sincos The sinus and cosinus (in this order) of the rotation angle
 * i_rot_center The coordinates of the rotation center in the input image
 * i_new_center The coordinate of the rotation center in the output image
 */

kernel void
rotate_reframe ( write_only image2d_t o_im,
		 read_only image2d_t i_im,
		 const sampler_t i_sampler,
		 const float2 i_sincos,
		 const float2 i_rot_center,
		 const float2 i_new_center)
{
  int idx = get_global_id (0);
  int idy = get_global_id (1);
  float x = (float)(idx) - i_new_center.x + 0.5f;
  float y = (float)(idy) - i_new_center.y + 0.5f;
  float2  coords;
  float val;

  coords = (float2) (i_sincos.y * x + i_sincos.x * y + i_rot_center.x,
		     - i_sincos.x * x + i_sincos.y * y + i_rot_center.y );
  val = read_imagef(i_im, i_sampler, coords).x;
  write_imagef(o_im, (int2)(idx, idy), (float4)(val, 0.0f, 0.0f, 1.0f));
}
